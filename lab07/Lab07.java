//package Lab07;
import java.util.Scanner;
import java.util.Random;
public class Lab07 {

    public static String Adjectives(){
        Random randomGenerator = new Random();
        //This statement generates random integers less than 10
        int randomInt = randomGenerator.nextInt(10);
        String adjective = "blue";
        switch (randomInt){
            case 1: adjective = "beautiful";
            break;
            case 2: adjective = "ugly";
            break;
            case 3: adjective = "pretty";
            break;
            case 4: adjective = "smart";
            break;
            case 5: adjective = "disgusting";
            break;
            case 6: adjective = "small";
            break;
            case 7: adjective = "big";
            break;
            case 8: adjective = "early";
            break;
            case 9: adjective = "important";
            break;
        }
       return adjective;
    }
    
    public static String subject(){
        Random randomGenerator = new Random();
        //This statement generates random integers less than 10
        int randomInt = randomGenerator.nextInt(10);
        String subject = "Lehigh";
        switch (randomInt){
            case 1: subject = "Ryan";
            break;
            case 2: subject = "Yami";
            break;
            case 3: subject = "Daniel";
            break;
            case 4: subject = "Gustavo";
            break;
            case 5: subject = "Omid";
            break;
            case 6: subject = "Grand Canyon";
            break;
            case 7: subject = "Niagra Falls";
            break;
            case 8: subject = "Madison";
            break;
            case 9: subject = "Luis";
            break;
        }
       return subject;
    } 
    
    public static String verb(){
         Random randomGenerator = new Random();
        //This statement generates random integers less than 10
        int randomInt = randomGenerator.nextInt(10);
        String verb = "said";
        switch (randomInt){
            case 1: verb = "had";
            break;
            case 2: verb = "was";
            break;
            case 3: verb = "went";
            break;
            case 4: verb = "walked";
            break;
            case 5: verb = "ran";
            break;
            case 6: verb = "came";
            break;
            case 7: verb = "played";
            break;
            case 8: verb = "caught";
            break;
            case 9: verb = "began";
            break;
        }
       return verb;
    }
    
    public static String object(){
        Random randomGenerator = new Random();
        //This statement generates random integers less than 10
        int randomInt = randomGenerator.nextInt(10);
        String object = "car";
        switch (randomInt){
            case 1: object = "car";
            break;
            case 2: object = "president";
            break;
            case 3: object = "proffesor";
            break;
            case 4: object = "plan";
            break;
            case 5: object = "computer";
            break;
            case 6: object = "camel";
            break;
            case 7: object = "potato";
            break;
            case 8: object = "food";
            break;
            case 9: object = "shirt";
            break;
        }
       return object;
    }
    public static String firstSentence(){
        String Subject = subject();
       
        System.out.println("The " + Adjectives() + " " +  Subject +  " " + verb() +  " " + "the " + Adjectives() + " " + object() + "." );
       return Subject;
    }
    public static void secondSentence(String Subject){
        System.out.println("This " + Subject + " was " + Adjectives() + " " +  Adjectives() +  " to " + Adjectives() + " " + object() + "." );
        System.out.println("It " +  "used" + " to " + verb() + " " +  subject() +  " at the " +  Adjectives() + " " + object() + "." );
        System.out.println("That " + Subject + " " + verb() + " her " +  object() +  "." );

    }
    
    
    
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        
        String Subject = firstSentence();
        System.out.println("Enter 1 if you want a new sentence");

        int newSentence = myScanner.nextInt();
         if (newSentence == 1){
            secondSentence(Subject);
         }
         else { System.out.println("Exit");}
        
        
        }

            
        

    }
    
