///
// Omid Ghazizadeh
// omg222
// 09/08/2018
// CSE 02 117
//Arithmetic.java
//
// compute the cost of the items you bought, 
// including the PA sales tax of 6%


public class Arithmetic {
  //// main method required for every Java program, called Arithmetic
  
  public static void main(String[] args) {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltPrice = 33.99;
    
    //the tax rate is PA
    double paSalesTax = 0.06;
    
    double totalCostOfPants;   //total cost of pants
    double totalCostOfShirts;  //total cost of shirts
    double totalCostOfBelts;   //total cost of belts
    double pantsTax;  //taxes on pants
    double shirtTax;  //taxes on shirts
    double beltTax;  //taxes on belts
    double totalCostOfPurchaseBefore; //total cost of the whole purchase before taxes
    double totalSalesTax;  //total tax charged on the whole purchase
    double totalCostOfPurchaseAfter;  //total cost fo the whole purchase after taxes
    
    ///////NOTICE HOW I MADE ALL MY VALUES INTO 2 DECIMAL VALUES BY 
    //multiplying it by 100, converting to an int, and then dividing the result by 100.0 

    totalCostOfPants = (int) (numPants * pantsPrice * ( 1 + paSalesTax) * 100) / 100.0;  // calculating the total cost of pants plus tax
    totalCostOfShirts = (int) (numShirts * shirtPrice * ( 1 + paSalesTax) * 100) / 100.0;  // calculating the total cost of shirts plus tax
    totalCostOfBelts = (int) (numBelts * beltPrice * ( 1 + paSalesTax) * 100) / 100.0;  //calculating the total cost of belts plus tax
    // calculating the total tax on pants
    //by multiplying number of pants by cost per pants and the tax
    pantsTax =  (int) (numPants * pantsPrice * paSalesTax * 100) / 100.0 ; 
    
    
    // calculating the total tax on shirts
    //by multiplying number of shirts by cost per shirt and the tax
    shirtTax =  (int) (numShirts * shirtPrice * paSalesTax * 100) / 100.0;
    
     // calculating the total tax on belts
    //by multiplying number of belts by cost per belt and the tax
    beltTax = (int) (numBelts * beltPrice * paSalesTax * 100) / 100.0;
    
    //calculating total cost of purchase before tax 
    //by subtracting the total of everything minus the total taxes
    totalCostOfPurchaseBefore = (totalCostOfPants + totalCostOfShirts + totalCostOfBelts) - (pantsTax + shirtTax + beltTax);
    
    //calculating the total sales tax by adding all the taxes for each item
    totalSalesTax = pantsTax + shirtTax + beltTax;
    
    //calculating the total cost of purchase after tax by adding all the costs of items
    totalCostOfPurchaseAfter = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    
    // printing out the Sales tax charged buying pants
    System.out.println("Sales tax charged for pants: $" + pantsTax );  
    // printing out the Sales tax charged buying shirts
    System.out.println("Sales tax charged for shirts: $" + shirtTax );
    // printing out the Sales tax charged buying belts
    System.out.println("Sales tax charged for belts: $" + beltTax );
    
    System.out.println("Total cost of pants: $" + totalCostOfPants ); // printing out the total cost of shirts bought plus tax
    System.out.println("Total cost of shirts: $" + totalCostOfShirts ); // printing out the total cost of shirts bought plus tax
    System.out.println("Total cost of belts: $" + totalCostOfBelts ); // printing out the total cost of shirts bought plus tax
    
    System.out.println("Total cost of purchase (before tax): $" + totalCostOfPurchaseBefore ); //printing out the total cost of purchase before tax
    System.out.println("Total Sales tax: $" + totalSalesTax);  //printing out the total sales tax
    System.out.println("Total cost of purchase (after tax): $" + totalCostOfPurchaseAfter ); //printing out the total paid for this transaction, including sales tax. 

  } 
  
}
    

    

    
    
    
  