/* Omid Ghaziazdeh      
  CSE 002 110
  Professor Carr
  10/12/18
  The purpose of this lab is to teach
  you nested loops and patterns that will 
  help you understand how to set up nested loops.
  PATTERN A
 */
package displayname;

import java.util.Scanner;
public class PatternA {

    
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        int input;
        System.out.println("Input an integer between 0 and 10:");
        while (!myScanner.hasNextInt()) {
            myScanner.next();
            System.out.println("Please input an integer only");
        }
        input = myScanner.nextInt();
        
        while (input > 10){ 
            System.out.println("Please input an integer between 0 and 10 only");
           input = myScanner.nextInt();
        }
        for (int numRows = 1; numRows <= input; numRows++){
            for (int numDisplay = 1; numDisplay <= numRows; numDisplay++ ){
                System.out.print(numDisplay + " ");
            }
            System.out.println();
        }
        
        
        
    }
    
}
