/// Omid Ghazizadeh
// 09/14/18
// CSE 002 110
// Check.java
/* Write a program that uses the Scanner class 
to obtain from the user the original cost of the check,
the percentage tip they wish to pay,
and the number of ways the check will be split.
Then determine how much each person in the group
needs to spend in order to pay the check */

import java.util.Scanner;

public class Check{
  // main method required for every Java program

  public static void main(String[] args){
    
   // tells Scanner that you are creating an instance that will take input from STDIN
    Scanner myScanner = new Scanner(System.in);
    
    // prompt the user for the original cost of the check
    System.out.print("Enter the original cost of check in format xx.xx: ");
    double checkCost = myScanner.nextDouble();
    
    //prompt the user for the tip percentage that they wish to pay and accept the input.
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    
    //prompt the user for the number of people that went to dinner
    System.out.print("Enter the number of people that attended the dinner: ");
    double numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars;  //whole dollar amount of cost  
    int dimes; 
    int pennies; //for storing digits
    //to the right of the decimal point 
    //for the cost$ 
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $ " + dollars + "." + dimes + pennies);
                       


    
    
    
  }
}
