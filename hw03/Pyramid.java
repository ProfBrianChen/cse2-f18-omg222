/*
 Omid Ghazizadeh
 omg222
 09/15/2018
 CSE 02 117
 Pyramid.java
 This homework gives you practice in writing code 
 that enables the user to input data and gives you
 practice in performing arithmetic operations.
 asks for dimensions of the pyramid and
 calculates the volume inside
*/

import java.util.Scanner; //importing the Scanner class

public class Pyramid{ // main method required for every Java program
  public static void main(String[] args){
     // declaring myScanner and we're going to receive input from the screen-construct
     Scanner myScanner = new Scanner(System.in); 
     
    // asking the user to input the square side of the pyrmid(length)
    System.out.print("The square side of the pyramid is (input length): ");
    // allows the user to input a double for the length of the pyramid
    double Length = myScanner.nextDouble();
    
    //asking the user to input the height of the pyramid
    System.out.print("The height of the pyramid is (input height): ");
    //allows the user to input a double for the height of the pyramid
    double Height = myScanner.nextDouble();
    
    //declaring Volume
    double Volume;
    //calculating the volume of pyramid by using the formula,
    //by squaring the length and multiplying it by height and dividing all by 3
    Volume = (Math.pow(Length, 2.0) * Height) / 3;
    // printing out the Volume
    System.out.println("The volume inside the pyramid is: " + Volume);
    
    
  }
}