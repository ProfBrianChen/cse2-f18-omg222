/*
 Omid Ghazizadeh
 omg222
 09/14/2018
 CSE 02 117
 Convert.java
 This homework gives you practice in writing code 
 that enables the user to input data and gives you
 practice in performing arithmetic operations.
 Converting acres times inches to cubic miles
*/
 
 import java.util.Scanner; //importing the Scanner class

 public class Convert { // main method required for every Java program
   public static void main(String[] args) { 
     
     // declaring myScanner and we're going to receive input from the screen-construct
     Scanner myScanner = new Scanner(System.in); 
     // asking the user to input a double for acres of land affected 
     System.out.print("Enter the affected area in acres, in form xx.xx: ");
     // allows the user to input a double for acres of land 
     double acresOfLand = myScanner.nextDouble();
     
     // asking the user to input how many inches of rain were dropped on average
     System.out.print("Enter the rainfall in the affected area in inches: ");
     // allows the user to input a double for inches of rain
     double inchesOfRain = myScanner.nextDouble();
     
     //declaring AcreFoot
     double AcreFoot;
     //declaring CubicMiles
     double CubicMiles;
     //calculating AcreFoot by turning inchesOfRain to Feet then multiplying by acresOfLand
     AcreFoot = ((inchesOfRain) / 12) * acresOfLand ; 
     
     // converting acreFoot into CubicMiles by using the formula
     CubicMiles = AcreFoot * 2.95928e-7 ;
     // printing out the result in cubic miles
     System.out.println( CubicMiles + " cubic miles");
     
   }
 }
