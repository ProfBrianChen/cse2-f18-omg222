/*
  Omid Ghaziazdeh      
  CSE 002
  Omid Ghazizadeh 110
  Professor Carr
  10/28/18
  This homework has the objective of giving
  you practice writing methods, manipulating strings 
  and forcing the user to enter good input.
*/
//package wordtools;
import java.util.HashMap; //importing hashMap class to use for method 5
import java.util.Map; //importing map class to use for method 5

import java.util.Scanner; //importing scanner class


public class hw07 {  //class
    public static String sampleText(){ //method 1
        Scanner myScanner = new Scanner(System.in); //declaring myscanner 
        System.out.println("Enter a sample text:"); // printing out statement 
        String Input = myScanner.nextLine(); //getting user input
        System.out.println("You Entered: " + Input); //printing user input again
        return Input; //returning it
    }
    public static char printMenu(String Input){ //method 2
        Scanner myScanner = new Scanner(System.in); //declaring myscanner
        //printing out the menu
        System.out.println("MENU\n" + 
        "c - Number of non-whitespace characters\n" +
        "w - Number of words\n" +
        "f - Find text\n" +
        "r - Replace all !'s\n" +
        "s - Shorten spaces\n" +
        "q - Quit");
        System.out.println("Choose an option: ");
        char choice = myScanner.nextLine().charAt(0);  //letting user input a character
        while (choice != 'q') { //what to do when the choice is not q
        switch(choice){ //using switch statements to dp the following letters
            //do the following if these charaters are inputted
            case 'q': System.out.print("Exit.");
            break;
            case 'c' : System.out.println("Number of non-whitespace characters:" +getNumOfNonWSCharacters(Input));
            break; 
            case 'w' : System.out.println("Number of words: " + getNumOfWords(Input));
            break;    
            case 'f' : System.out.println("instances:" + findText(Input));
            break;    
            case 'r' : System.out.println("Edited text: " +replaceExclamation(Input));
            break;    
            case 's' : System.out.println("Edited Text: " + shortenSpace(Input));
            break;                
            default: System.out.println("Invalid Entry!"); //going thru while loop again if user doesnt input inpu any of the following characters
        }
            choice = myScanner.nextLine().charAt(0);
        
        }
        return choice; //returning the char
                }
    public static int getNumOfNonWSCharacters(String Input){ //method 3
       int numSpaces = 0; //declaring number of spaces
       int count = Input.length();//the length of the input is count
       for (int x = Input.length()-1; x > 0; x--){ //for loop to increase numspaces each time a space is seen
           if (Input.charAt(x) == ' '){
               numSpaces++;
           }
    }
       return count - numSpaces; //then subtrating the length of original input minus the spaces
    }
    
    public static int getNumOfWords(String Input){ //method 4
       int numSpaces = 0; //declaring number of spaces
       for (int x = Input.length()-1; x > 0; x--){//for loop to increase numspaces each time a space is seen
           if (Input.charAt(x) == ' '){ 
               numSpaces++;
           }
    }
       return numSpaces + 1; //number of words is spaces +1
    }
    
    public static int findText(String Input){ //method 5
        
        int count = 0; //declaring a counter
        String[] words = Input.split(" "); //declaring words as an array of Input
        Scanner myScanner = new Scanner(System.in); //declaring myscanner 
        System.out.println("Enter a word or phrase to be found:"); //asking a user to enter a word
        String text = myScanner.nextLine(); //getting the user input
      
            Map<String, Integer> frequency = new HashMap<String, Integer>(); //declaring the Map and hashMap 
            for (String word : words){ //comparing word to words
                Integer h = frequency.get(word); //declaring h as frequency of word
                //checking if null
                if (h == null)
                    h = 0;
                frequency.put(word, h + 1); //adding one to h and counting frequency in input
            }
            return frequency.get(text); //returning the frequency of text
            
    }
    public static String replaceExclamation(String Input){ //method 6
        String newString = "no Exclamation point"; //a string to begin with, to declare
         for (int x = Input.length()-1; x > 0; x--){ //for loop to go thru the whole input text
           if (Input.charAt(x) == '!'){ //if there is a character ! in the input
               newString = Input.replace("!", "."); //replace it with a .
           }
    }
         return newString; //return the string
    }
     public static String shortenSpace(String Input){ //method 7
        String newString = "no double spaces"; //a string to begin with, to declare
         for (int x = Input.length()-1; x > 0; x--){ //for loop to go thru the whole input text
           if (Input.charAt(x) == ' '){ // if there is a "  " in the input
               newString = Input.replace("  ", " "); //replace it with a " "
           }
    }
         return newString; //return that string
    }
    
   
    public static void main(String[] args) { //main method
        String Input = sampleText(); // initializing the input user puts in sampleText method
        printMenu(Input); //going to the menu with the input "Input"
        

            
        
    }
    
}
