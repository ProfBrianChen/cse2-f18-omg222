/* Prof AKC
	September 9, 2018
	An introduction to the Scanner class*/

import java.util.Scanner;

public class scannerIntroduction{
	public static void main(String[] args){
		Scanner myScanner;
		
		myScanner = new Scanner(System.in); 
		// In order:
		//(name of the instance) = ("new" operator) (Constructor) (input for the constructor)
		// The "input" System.in tells the compiler what 
		//   you're going to do with your Scanner (here,
		//   we're going to receive input from the screen)
		
		// Ask the user to input an integer
		System.out.println("Please input an integer A: ");
		// nextInt() allows the user to input an integer
		int integerA = myScanner.nextInt();

		// Ask the user to input another integer
		System.out.println("Please input an integer B: ");
		int integerB = myScanner.nextInt();
          
		// Add the two integers and output them to the screen 
		System.out.println("A + B = ");
		System.out.println(integerA + integerB);
		myScanner.nextLine(); // Ignore the remainder of the
		                      // line so we can input other text
		
		// Ask the user to input her name
		System.out.println("Now please enter your name: ");
		// nextLine() allows the user to input a string
		String myName = myScanner.nextLine();
		
		System.out.println("Hello, " + myName);
		
	}
	
}