import java.util.Scanner;

public class comparisonsAndBooleans{
	public static void main(String[] args){
		Scanner myScanner = new Scanner(System.in);
		double myValue1 = 2.0;
		double myValue2 = 1.3;
		boolean myValue1IsBigger = myValue1 > myValue2;
		System.out.println("The truth value of myValue1IsBigger: " + myValue1IsBigger);
		
		myScanner.nextLine();
		
		boolean isVal1StillBigger = myValue1 > myValue2+1;
		// This final expression gets evaluated as follows:
		// bool val1IsMuchBigger= myVal1 > (myVal2+1);
		System.out.println("The truth value of isVal1StillBigger: " + isVal1StillBigger);
		
		myScanner.nextLine();
		
		// Let's examine associativity now
		//boolean complexCompare1 = myValue1 < myValue2 == (myValue1-0.7);
		// Why does this fail?
		
		boolean myBool1 = true, myBool2 = false, myBool3 = true, myBool4 = false;
		boolean complexCompare2 = ((myBool1 == myBool2) != myBool3) == myBool4;
		// (((myBool1 == myBool2) != myBool3) == myBool4);
		System.out.println("The truth value of our complex comparison is: " + complexCompare2);
		
		myScanner.nextLine();
		
		//What if we try to compare different types?
		//boolean tryCompDiffTypes = myValue1 != "J-E-T-S";
		
		myScanner.nextLine();
		//Logical operators
		int age = 20;
		boolean hasCreditCard = true;
		boolean canRentCar = age >= 25 || hasCreditCard == true;
    //This gets evaluated in the following way:
		//  boolean identityVerified = ((age == 25) \&\& (hasCreditCard == true));
		System.out.println("This person can rent a car: " + canRentCar);
		
		myScanner.nextLine();
		
		// Equality of strings
		String string1 = "Hello";
		String string2 = "Hello";
		String string3 = "He";
		String string4 = "llo";
		
		System.out.println("The first and second string are equal: " + (string1 == string2));
		
		myScanner.nextLine();
		
		String string5 = string3 + string4;
		System.out.println("This is string5: " + string5);
		System.out.println("This is string1: " + string1);
		
		myScanner.nextLine();
		
		System.out.println("The first and fifth string are equal: " + (string1 == string5));
		
		myScanner.nextLine();
		
		System.out.println("The first and fifth string are equal: " + string1.equals(string5));
		
	}
	
}