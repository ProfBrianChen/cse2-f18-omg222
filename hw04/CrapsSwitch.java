/*
 Omid Ghazizadeh
 omg222
 09/22/2018
 CSE 02 117
 CrapsIf.java
 The classic casino game, Craps, involves
 the rolling of two six sided dice and evaluating
 the result of the roll. Among other things, 
 craps is notable for producing slang terminology for
 describing the outcome of a roll of two dice.
 by using switch statements.
*/

 import java.util.Scanner; //importing the Scanner class
 import java.util.Random; //importing the Random class

 public class CrapsSwitch{
    public static void main(String[] args){ 
     int diceCast; //declaring diceCast
     int dice1 = -1; //declaring and initializing dice1
     int dice2 = -1; //declaring and initializing dice2
   
     // declaring myScanner and we are going to receive input from the screen-construct
     Scanner myScanner = new Scanner(System.in); 
     // Ask the user if they would like randomly cast dice or if they would like to state the two dice they want to evaluate
     System.out.println("if you would like to randomly cast dice, type 1. If you would like to state the two dice you want to evaluate, type 2. ");
     diceCast = myScanner.nextInt(); //allowing the user to input 1 or 2 for the type of dice casting
     
     switch (diceCast) {
       case 1 : 
             dice1 = (int) (Math.random()*6 + 1); // randomly generating a number between 1 and 6
             dice2 = (int) (Math.random()*6 + 1); // randomly generating another number between 1 and 6
             break;
      case 2 :
            System.out.println("Please input what number you want your first dice to be (between 1 and 6 inclusive). "); //asking user to input
            dice1 = myScanner.nextInt(); //lets user input number for the first dice 
            switch (dice1){
                  case 1: break; //if user is put is 1, do nothing
                  case 2: break; //if user is put is 2, do nothing
                  case 3: break; //if user is put is 3, do nothing
                  case 4: break; //if user is put is 4, do nothing                 
                  case 5: break; //if user is put is 5, do nothing
                  case 6: break; //if user is put is 6, do nothing
                  default: //if user input does not lie within 1 to 6 inclusive                             
                    System.out.print("Please try again (Enter a value from 1 to 6): "); //prompts user to re-enter a number from 1 to 6
                    dice1 = myScanner.nextInt(); //stores input from above line
                    break;
                }
            System.out.println("Please input what number you want your second dice to be (between 1 and 6 inclusive). ");
            dice2 = myScanner.nextInt(); //lets user input number for the second dice 
            switch (dice2){
                  case 1: break; //if user is put is 1, do nothing
                  case 2: break; //if user is put is 2, do nothing
                  case 3: break; //if user is put is 3, do nothing
                  case 4: break; //if user is put is 4, do nothing                 
                  case 5: break; //if user is put is 5, do nothing
                  case 6: break; //if user is put is 6, do nothing
                  default: //if user input does not lie within 1 to 6 inclusive                             
                    System.out.print("Please try again (Enter a value from 1 to 6): "); //prompts user to re-enter a number from 1 to 6
                    dice2 = myScanner.nextInt(); //stores input from above line
                    break;
                }
     }
      
      switch (dice1) {
         
          case 1: //if the first dice rolled 1
            switch(dice1){ 
              case 1: System.out.println("You rolled: Snake Eyes!"); //prints out result if rolled 1,1 
                      break;
              case 2: System.out.println("You rolled: Ace Deuce!"); // prints out result if rolled 1,2
                      break;
              case 3: System.out.println("You rolled: Easy Four!"); //prints out result if rolled 1,3
                      break;
              case 4: System.out.println("You rolled: Fever Five!"); //prints out result if rolled 1,4
                      break;
              case 5: System.out.println("You rolled: Easy Six!"); //prints out result if rolled 1,5
                      break;
              case 6: System.out.println("You rolled: Seven Out"); //prints out result if rolled 1,6
                      break;
              default: System.out.println("You didn't choose a number between 1 and 6 inclusive for one or both of the dice. "); //prints out result if inputs do not lie within 1,6 inclusive
                       break;
           }
          break;
        
       case 2: //if the first dice rolled 2
            switch(dice2){
              case 1: System.out.println("You rolled: Ace Deuce!"); // prints out result if rolled 2,1
                      break;
              case 2: System.out.println("You rolled: Hard Four!"); //prints out result if rolled 2,2
                      break;
              case 3: System.out.println("You rolled: Fever Five!"); //prints out result if rolled 2,3
                      break;
              case 4: System.out.println("You rolled: Easy Six!"); //prints out result if rolled 2,4
                      break;
              case 5: System.out.println("You rolled: Seven Out"); //prints out result if rolled 2,5
                      break;
              case 6: System.out.println("You rolled: Easy Eight"); //prints out result if rolled 2,6
                      break;
              default: System.out.println("You didn't choose a number between 1 and 6 inclusive for one or both of the dice. "); //prints out result if inputs do not lie within 1,6 inclusive
                       break;
           }
          break;
        
        
          case 3: //if the first dice rolled 3
            switch(dice2){
              case 1: System.out.println("You rolled: Easy Four!"); //prints out result if rolled 3,1
                      break;
              case 2: System.out.println("You rolled: Fever Five!"); //prints out result if rolled 3,2
                      break;
              case 3: System.out.println("You rolled: Hard Six!"); //prints out result if rolled 3,3
                      break;
              case 4: System.out.println("You rolled: Seven Out!"); //prints out result if rolled 3,4
                      break;
              case 5: System.out.println("You rolled: Easy Eight!"); //prints out result if rolled 3,5
                      break;
              case 6: System.out.println("You rolled: Nine"); //prints out result if rolled 3,6
                      break;
              default: System.out.println("You didn't choose a number between 1 and 6 inclusive for one or both of the dice. "); //prints out result if inputs do not lie within 1,6 inclusive
                       break;
  
            }
          break;
          
          case 4: //if the first dice rolled 4
            switch(dice2){
              case 1: System.out.println("You rolled: Fever Five!"); //prints out result if rolled 4,1
                      break;
              case 2: System.out.println("You rolled: Easy Six!"); //prints out result if rolled 4,2
                      break;
              case 3: System.out.println("You rolled: Seven Out!"); //prints out result if rolled 4,3
                      break;
              case 4: System.out.println("You rolled: Hard Eight!"); //prints out result if rolled 4,4
                      break;
              case 5: System.out.println("You rolled: Nine!"); //prints out result if rolled 4,5
                      break;
              case 6: System.out.println("You rolled: Easy Ten"); //prints out result if rolled 4,6
                      break;
              default: System.out.println("You didn't choose a number between 1 and 6 inclusive for one or both of the dice. "); //prints out result if inputs do not lie within 1,6 inclusive
                       break;
           }
          break;
      
          case 5: //if first dice rolled 5
            switch(dice2){
              case 1: System.out.println("You rolled: Easy Six!"); //prints out result if rolled 5,1
                      break;
              case 2: System.out.println("You rolled: Seven Out"); //prints out result if rolled 5,2
                      break;
              case 3: System.out.println("You rolled: Easy Eight!"); //prints out result if rolled 5,3
                      break;
              case 4: System.out.println("You rolled: Nine!"); //prints out result if rolled 5,4
                      break;
              case 5: System.out.println("You rolled: Hard Ten"); //prints out result if rolled 5,5
                      break;
              case 6: System.out.println("You rolled: Yo-Leven"); //prints out result if rolled 5,6
                      break;
              default: System.out.println("You didn't choose a number between 1 and 6 inclusive for one or both of the dice. "); //prints out result if inputs do not lie within 1,6 inclusive
                       break;
           }
          break;
          case 6: //if first dice rolled 6
            switch(dice2){
              case 1: System.out.println("You rolled: Seven Out"); //prints out result if rolled 6,1
                      break;
              case 2: System.out.println("You rolled: Easy Eight"); //prints out result if rolled 6,2
                      break;
              case 3: System.out.println("You rolled: Nine"); //prints out result if rolled 6,3
                      break;
              case 4: System.out.println("You rolled: Easy Ten");//prints out result if rolled 6,4
                      break;
              case 5: System.out.println("You rolled: Yo-Leven"); //prints out result if rolled 6,5
                      break;
              case 6: System.out.println("You rolled: Boxcars"); //prints out result if rolled 6,6
                      break;
              default: System.out.println("You didn't choose a number between 1 and 6 inclusive for one or both of the dice. "); //prints out result if inputs do not lie within 1,6 inclusive
                       break;
           }
          default : System.out.println("You didn't choose a number between 1 and 6 inclusive for one or both of the dice. ");
          break;
      }
    }
 }
   
      
      
       