/*
 Omid Ghazizadeh
 omg222
 09/22/2018
 CSE 02 117
 CrapsIf.java
 The classic casino game, Craps, involves
 the rolling of two six sided dice and evaluating
 the result of the roll. Among other things, 
 craps is notable for producing slang terminology for
 describing the outcome of a roll of two dice.
 by using if statements.
*/
 
 import java.util.Scanner; //importing the Scanner class
 import java.util.Random; //importing the Random class

 public class CrapsIf{
    public static void main(String[] args){ 
     int diceCast; //declaring diceCast
     int dice1 = -1; //declaring dice1
     int dice2 = -1; //declaring dice2
   
     // declaring myScanner and we are going to receive input from the screen-construct
     Scanner myScanner = new Scanner(System.in); 
     // Ask the user if they would like randomly cast dice or if they would like to state the two dice they want to evaluate
     System.out.println("if you would like to randomly cast dice, type 1. If you would like to state the two dice you want to evaluate, type 2. ");
     diceCast = myScanner.nextInt(); //allowing the user to input 1 or 2 for the type of dice casting
     
     
     
     if (diceCast == 1){ //entering the if statement If they want to randomly cast dice
       dice1 = (int) (Math.random()*6 + 1); // randomly generating a number between 1 and 6
       dice2 = (int) (Math.random()*6 + 1); // randomly generating another number between 1 and 6
       
       
       
     }
     
     else if (diceCast == 2){
       //Scanner anotherScanner = new Scanner(System.in); //creating another scanner to get input for the two dice numbers
       System.out.println("Please input what number you want your first dice to be (between 1 and 6 inclusive). "); //asking user to input
       dice1 = myScanner.nextInt(); //lets user input number for the first dice 
       if (dice1 > 6){ //gonig through an if statement if a number greater than 6 is inputted
         System.out.println("Please input an integer between 1 and 6 only"); //asking user to input again 
         dice1 = myScanner.nextInt(); //letting user to input again for dice 1
       }
       System.out.println("Please input what number you want your second dice to be (between 1 and 6 inclusive). ");
       dice2 = myScanner.nextInt(); //lets user input number for the second dice 
        if (dice2 > 6){  //gonig through an if statement if a number greater than 6 is inputted
         System.out.println("Please input an integer between 1 and 6 only"); //asking to input again
         dice2 = myScanner.nextInt(); //letting user to input again for dice 2
       
     } 
   }
     
     if (dice1 == 1 && dice2 == 1){ //if rolled 1,1
      System.out.println("You rolled: Snake Eyes!"); //prints out result
    } 
     else if ((dice1 == 1 && dice2 == 2) || (dice1 == 2 && dice2 == 1)){ //if rolled 1,2
      System.out.println("You rolled: Ace Deuce!"); // prints out result
    } 
      else if (dice1 == 2 && dice2 == 2){ //if rolled 2,2
      System.out.println("You rolled: Hard Four!"); //prints out result
    } 
      else if ((dice1 == 1 && dice2 == 3) || (dice1 == 3 && dice2 == 1)){ //if rolled 1,3
      System.out.println("You rolled: Easy Four!"); //prints out result
    } 
      else if ((dice1 == 3 && dice2 == 2) || (dice1 == 2 && dice2 == 3)){ //if rolled 2,3
      System.out.println("You rolled: Fever Five!"); //prints out result
    } 
     else if (dice1 == 3 && dice2 == 3){ //if rolled 3,3
      System.out.println("You rolled: Hard Six!"); //prints out result
    } 
      else if ((dice1 == 1 && dice2 == 4) || (dice1 == 4 && dice2 == 1)){ //if rolled 1,4
      System.out.println("You rolled: Fever Five!"); //prints out result
    } 
     else if ((dice1 == 4 && dice2 == 2) || (dice1 == 2 && dice2 == 4)){ //if rolled 2,4
      System.out.println("You rolled: Easy Six!"); //prints out result
    } 
      else if ((dice1 == 3 && dice2 == 4) || (dice1 == 4 && dice2 == 3)){ //if rolled 3,4
      System.out.println("You rolled: Seven Out!"); //prints out result
    } 
      else if (dice1 == 4 && dice2 == 4){ //if rolled 4,4
      System.out.println("You rolled: Hard Eight!"); //prints out result
    } 
      else if ((dice1 == 1 && dice2 == 5) || (dice1 == 5 && dice2 == 1)){ //if rolled 1,5
      System.out.println("You rolled: Easy Six!"); //prints out result
    } 
      else if ((dice1 == 5 && dice2 == 2) || (dice1 == 2 && dice2 == 5)){ //if rolled 2,5
      System.out.println("You rolled: Seven Out"); //prints out result
    } 
     else if ((dice1 == 3 && dice2 == 5) || (dice1 == 5 && dice2 == 3)){ //if rolled 3,5
      System.out.println("You rolled: Easy Eight!"); //prints out result
    } 
      else if ((dice1 == 4 && dice2 == 5) || (dice1 == 5 && dice2 == 4)){ //if rolled 4,5
      System.out.println("You rolled: Nine!"); //prints out result
    } 
      else if (dice1 == 5 && dice2 == 5){ //if rolled 5,5
      System.out.println("You rolled: Hard Ten"); //prints out result
    } 
      else if ((dice1 == 1 && dice2 == 6) || (dice1 == 6 && dice2 == 1)){ //if rolled 1,6
      System.out.println("You rolled: Seven Out"); //prints out result
    } 
      else if ((dice1 == 6 && dice2 == 2) || (dice1 == 2 && dice2 == 6)){ //if rolled 2,6
      System.out.println("You rolled: Easy Eight"); //prints out result
    } 
      else if ((dice1 == 6 && dice2 == 3) || (dice1 == 3 && dice2 == 6)){ //if rolled 3,6
      System.out.println("You rolled: Nine"); //prints out result
    } 
      else if ((dice1 == 4 && dice2 == 6) || (dice1 == 6 && dice2 == 4)){ //if rolled 4,6
      System.out.println("You rolled: Easy Ten");//prints out result
    } 
      else if ((dice1 == 5 && dice2 == 6) || (dice1 == 6 && dice2 == 5)){ //if rolled 5,6
      System.out.println("You rolled: Yo-Leven"); //prints out result
    } 
      else if (dice1 == 6 && dice2 == 6){ //if rolled 6,6
      System.out.println("You rolled: Boxcars"); //prints out result
    } 
     else { //if input is'nt 1 or 2
      System.out.println("You didn't choose either 1 or 2 to cast your dice. Please try again later!"); //prints out result
    }
     
 } 
}
  