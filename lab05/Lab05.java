 /// Omid Ghazizadeh
// 10/05/18
// CSE 002 110
// Lab05.java
//Your program is to write loops that
//asks the user to enter information 
//relating to a course they are currently taking

import java.util.Scanner; 
public class Lab05 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
    int courseNum ;
    String departName;
    int numTimes = 0;
    int timeOf = 0;
    String instName;
    int numStudents = 0;
    String safety;
    
    System.out.println("Enter your class course number: ");
    
    while (! myScanner.hasNextInt()) {
      System.out.println("you need to input an integer type!");
         safety = myScanner.nextLine() ;
      
    }
     courseNum = myScanner.nextInt();
    System.out.println("Enter your department name: ");
   
    while (! myScanner.hasNext()) {
      System.out.println("you need to input a String type!");
         safety = myScanner.nextLine() ;
      
    }
      departName = myScanner.next();
     System.out.println("Enter number of times the class meets per week: ");
     
    while (! myScanner.hasNextInt()) {
      
      System.out.println("you need to input an Integer type!");
         safety = myScanner.nextLine() ;
    }
    numTimes = myScanner.nextInt();
    
    System.out.println("Enter the instructor's name: ");
     while (! myScanner.hasNext()) {
      System.out.println("you need to input a String type!");
         safety = myScanner.nextLine() ;
      
    }
      instName = myScanner.next();
      
     System.out.println("Enter the number of students in the class: ");
     while (! myScanner.hasNextInt()) {
      
      System.out.println("you need to input an Integer type!");
         safety = myScanner.nextLine() ;
    }
    numStudents = myScanner.nextInt();
    
    System.out.println("Class course number: " +courseNum+ "   Department name: " + departName+ "    Number of times the class meets per week: " +numTimes+ "   Instructor's name: " +instName+ "   Number of students in the class: ");
      
      
  }

}
    
