/*
  Omid Ghaziazdeh      
  CSE 002
  Omid Ghazizadeh 110
  Professor Carr
  12/02/18
  Write a program that will simulate the game of Tic-tac-toe.
*/
//package hw10.java;
import java.util.*;

public class hw10 {

    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in); //scanner class
        int row = 0; 
        int column = 0;
        char player = 'X'; //player initialized to X
        char[][] arr = new char[3][3]; //array of row 3, column 3
        arr[0][0] = '1'; //array inputs
        arr[0][1] = '2';
        arr[0][2] = '3';
        arr[1][0] = '4';
        arr[1][1] = '5';
        arr[1][2] = '6';
        arr[2][0] = '7';
        arr[2][1] = '8';
        arr[2][2] = '9';
        System.out.println("Player O is \"O\" and Player X is \"X\"");
        for (int i = 0; i < arr.length; i++) { //for loop to print initial array
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        while (win(arr) == false) { //while there's no win
            System.out.println("Player " + player + " : Put in the row number followed by enter, then column number.");
            while (true) { 

                while (true) {
                    if (myScanner.hasNextInt()) { // begings if statement to check if user input is an int
                        row = myScanner.nextInt();
                        if (row < 3 && row >= 0) { // if statement for if input is within range
                            break;
                        } else {
                            System.out.println("Inputs have to be within the range of 0-2. Try again.");
                             myScanner.nextInt();
                        }
                    } else {
                        System.out.println("Inputs have to be integers. Try again.");
                        myScanner.next();
                    }
                }
                while (true) { //same exact thing to check for input column
                    if (myScanner.hasNextInt()) { 
                        column = myScanner.nextInt();
                        if (column < 3 && column >= 0) { // if statement for if input is within range
                            break;
                        } else {
                            System.out.println("Inputs have to be within the range of 0-2. Try again.");
                            column = myScanner.nextInt();
                        }
                    } else {
                         System.out.println("Inputs have to be integers. Try again.");
                        myScanner.next();
                    }
                    
                }

                if (arr[row][column] == 'X' || arr[row][column] == 'O') { //error message if spot is taken
                    System.out.println("Error. Element number is already chosen. Try again.");
                    continue;
                }
                break;
            }
            arr[row][column] = player; //put down X or O in the chosen spot
            for (int i = 0; i < arr.length; i++) { //print the array with X and/or O
                for (int j = 0; j < arr[i].length; j++) {
                    System.out.print(arr[i][j] + " ");
                }
                System.out.println();
            }
            if (win(arr)) { //if win returns true then win statement appears
                System.out.println("Player " + player + " is the winner!");
            }

            if (player == 'O') { //switch player 
                player = 'X';
            } else {
                player = 'O';
            }
           //if (win(arr) == false) {
            //    System.out.println("The game is a tie.");
            //}
            
             if (!hasEmptySpot(arr)){ //check if there's a tie when hasEmptySpot is false
                System.out.println("The game is a tie.");
                return; 
             }
             
        }
         

    }

    public static Boolean win(char[][] arr) { //win method 
        Boolean occupied = true; //check all the scenarios for a win
        for (int i = 0; i < arr.length; i++) { //go through the whole array
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] != 'O' || arr[i][j] != 'X') {
                    occupied = false;
                }

            }
        }
        //if (occupied) {
         //   return false;
        //}
        return (arr[0][0] == arr[0][1] && arr[0][0] == arr[0][2]) //all the possible ways to win
                || (arr[0][0] == arr[1][1] && arr[0][0] == arr[2][2])
                || (arr[0][0] == arr[1][0] && arr[0][0] == arr[2][0])
                || (arr[2][0] == arr[2][1] && arr[2][0] == arr[2][2])
                || (arr[2][0] == arr[1][1] && arr[2][0] == arr[0][2])
                || (arr[0][2] == arr[1][2] && arr[0][2] == arr[2][2])
                || (arr[0][1] == arr[1][1] && arr[0][1] == arr[2][1])
                || (arr[1][0] == arr[1][1] && arr[1][0] == arr[1][2]);
    }
    
    public static Boolean hasEmptySpot(char[][] arr){
   //loop and check if there is any numbers left 
    for (int i = 0; i< arr.length; i++) { //go through the whole array
        for (int j = 0; j < arr[0].length; j++) {
            if (arr[i][j] != 'O' && arr[i][j] != 'X') {
                return true;
            }
        }
    }
    return false; //return false if all spots are taken 
}

}
