//////////
// Omid Ghazizadeh 
// omg222
/// CSE 02 110 WelcomeClass
///

public class WelcomeClass{
  
  public static void main(String args[]){
    /// Print the following pattern with lehigh username to terminal window
    System.out.println(" ----------- ");
    System.out.println("  | WELCOME |  ");
    System.out.println(" ----------- ");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println(" <-O--M--G--2--2--2-> ");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println(" v  v  v  v  v  v ");
  }
  
}