/////
// Cyclometer.java
// Omid Ghazizadeh
// 09/07/18
// CSE02 117 
///
// records two kinds of data, the time elapsed in seconds
// and the number of rotations of the front wheel during that time

public class Cyclometer {
  //// main method required for every Java program
  
  public static void main( String[] args){
    
    // variables that will store the number of seconds
    //for each trip and the number of counts (rotations) for each trip
    int secsTrip1 = 480;  // declare and initialize secTrip1 to 480
   	int secsTrip2 = 3220;  //  declare and initialize secTrip2 3220
		int countsTrip1 = 1561;  //  declare and initialize countsTrip1 1561
		int countsTrip2 = 9037; //  declare and initialize countsTrip2 9037
    
    //variables for useful constants and for storing the various distances
    double wheelDiameter=27.0;  //
  	double PI = 3.14159; // initializing PI
  	int feetPerMile = 5280;  // initializing feetPerMile
  	int inchesPerFoot = 12;   // initializng inchesPerFoot
  	int secondsPerMinute = 60;  // initializing secondsPerMinute
	  double distanceTrip1; //declare
    double distanceTrip2; // declare
    double totalDistance;  // declare 
    
    //printing out the numbers that have been stored in the variables
    //that store number of seconds (converted to minutes) and the counts
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute)+" minutes and had " + countsTrip1 + " counts.");
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
    
		//run the calculations; store the values. Document your
		//calculation here. Calculating distanceTrip1 by 
    //multiplying trip counts by the wheel diameter and pi
	distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
  distanceTrip2 = (countsTrip2 * wheelDiameter * PI) / (inchesPerFoot * feetPerMile); //calculating distanceTrip2 and turning in to miles 
	totalDistance = distanceTrip1 + distanceTrip2; //finding total distance by adding the two trip's distances 
    
    //Print out the output data.
  System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	System.out.println("Trip 2 was " + distanceTrip2 + " miles");
	System.out.println("The total distance was " + totalDistance + " miles");
    
  }
  
}
 




  

    
    
  