//package hw08;
import java.util.Scanner; //importing Scanner class
import java.util.Random; //importing Random class

public class Shuffling{ //Shuffling class
public static void main(String[] args) { //main method
Scanner scan = new Scanner(System.in);  //delcaring scanner class
 //an array of suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"}; 
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //an array of all the name ranks
String[] cards = new String[52]; //creating array cards with size 52
String[] hand = new String[5];  //creating array hand with size 5
int numCards = 5;  //number of cards when hand is drawn is 5
int again = 1; //initializing integer again to 1
int index = 51; //integer index to 51
for (int i=0; i<52; i++){  //for loop to print cards elements with both suitNames and rankNames arrays in them
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); //printing it out
} 
System.out.println(); //going to the next line
printArray(cards); //go to method printArray with input cards array
shuffle(cards); //go to method shuffle with input cards array
printArray(cards); //go to method printArray with input cards array
while(again == 1){ //while loop that will go through cuz again equals to 1
   hand = getHand(cards,index,numCards); //setting array hand equal to method getHand
   printArray(hand); //go to method printArray with input hand array
   index = index - numCards; //subtract numCards(5) from index each time a hand is picked and set it equal to index
   System.out.println("Enter a 1 if you want another hand drawn"); //print out the following
   again = scan.nextInt();  //getting input from user
}
}
public static void printArray(String[] hello){ //printArray method
    for (int i = 0; i < hello.length; i++){ //for loop to printout the original array with a space between each element
        System.out.print(hello[i]+ " ");}
   System.out.println();  
}
  
public static void shuffle(String[] cards){ //shuffle method
    Random randomGenerator = new Random(); //declaring randmGenenrator
    String deck; //declaring deck
    for (int i = 0; i < 100; i++ ){ //for loop to shuffle the array
          int generated = randomGenerator.nextInt(50); //integer generated equals to random number between 0 and 49
          deck = cards[0]; //deck is equals to the firt element in the cards array
          cards[0] = cards[generated]; //first element in cards is equal to the random number generated element in the cards array
          cards[generated] = deck; //deck is then set as the value of that element
      }
} 

public static String[] getHand(String[] cards, int index, int numCards) { //method getHand
    String[] hand = new String[numCards]; //array hand
    for (int i = 0; i < numCards ; i++){ //for loop to assign values to only 5 elements in the array
        hand[i] = cards[index-i]; 
    }
    return hand; //returning it cuz well need this value
}
}

