 /// Omid Ghazizadeh
// 09/21/18
// CSE 002 110
// CardGenerator.java
/* program that will pick a random card
from the deck so you can practice your tricks alone.*/

import java.util.Random;

public class CardGenerator{
  public static void main(String[] args){
    
    Random CardNumber = new Random();
    int cardNumber = (int) (Math.random()*52 + 1);
    int Club;
    int Diamond;
    int Hearts; 
    int Spade;
    int Jack = 11;
    int Queen= 12;
    int King= 0;
    int Ace= 1;
    final int NUMSUITS = 13;
    

   
      if (cardNumber <= 13 ) {

          if ( cardNumber % NUMSUITS == 11){
              cardNumber = Jack;
             System.out.println("You picked the Jack of diamonds" );
          }
          else if ( cardNumber % NUMSUITS == 12){
            cardNumber = Queen;
            System.out.println("You picked the Queen of diamonds" );
          }
          else if ( cardNumber % NUMSUITS == 0){
            cardNumber = King;
            System.out.println("You picked the King of diamonds" );
          }
          else if ( cardNumber % NUMSUITS == 1){
             cardNumber = Ace;
             System.out.println("You picked the Ace of diamonds" );
          }
          else{
            System.out.println("You picked the " + cardNumber % NUMSUITS + " of diamonds" );
          }
          
    }
    
      else if (cardNumber <= 26 ) {
       
         if ( cardNumber % NUMSUITS == 11){
              cardNumber = Jack;
             System.out.println("You picked the Jack of clubs" );
        }
        else if ( cardNumber % NUMSUITS == 12){
            cardNumber = Queen;
            System.out.println("You picked the Queen of clubs" );
        }
        else if ( cardNumber % NUMSUITS == 0){
            cardNumber = King;
            System.out.println("You picked the King of clubs" );
        }
        else if ( cardNumber % NUMSUITS == 1){
             cardNumber = Ace;
             System.out.println("You picked the Ace of clubs" );
        }
         
      else{
          System.out.println("You picked the " + cardNumber % NUMSUITS + " of clubs" );
          }
    }
        
      else if (cardNumber <= 39 ) {
       
         if ( cardNumber % NUMSUITS == 11){
              cardNumber = Jack;
             System.out.println("You picked the Jack of hearts" );
        }
        else if ( cardNumber % NUMSUITS == 12){
            cardNumber = Queen;
            System.out.println("You picked the Queen of hearts" );
        }
        else if ( cardNumber % NUMSUITS == 0){
            cardNumber = King;
            System.out.println("You picked the King of hearts" );
        }
        else if ( cardNumber % NUMSUITS == 1){
             cardNumber = Ace;
             System.out.println("You picked the Ace of hearts" );
        }
      else{
         System.out.println("You picked the " + cardNumber % NUMSUITS + " of hearts" );
         }
    }
      else if (cardNumber <= 52 ) {
       
         if ( cardNumber % NUMSUITS == 11){
              cardNumber = Jack;
             System.out.println("You picked the Jack of spades" );
        }
       else if ( cardNumber % NUMSUITS == 12){
            cardNumber = Queen;
            System.out.println("You picked the Queen of spades" );
        }
       else if ( cardNumber % NUMSUITS == 0){
            cardNumber = King;
            System.out.println("You picked the King of spades" );
        }
       else if ( cardNumber % NUMSUITS == 1){
             cardNumber = Ace;
             System.out.println("You picked the Ace of spades" );
        }
      else{
        System.out.println("You picked the " + cardNumber % NUMSUITS + " of spades" );
        }
    }
  }
}