/// Omid Ghazizadeh
// 10/09/18
// CSE 002 110
// Hw05.java
/*your program should calculate the probabilities
of each of the four hands described above by
computing the number of occurrences divided
by the total number of hands. */

import java.util.Scanner; //importing scanner class
public class Hw05 { //  
 //main method required for every Java program, called Hw05
   
    public static void main(String[] args) { 
        Scanner myScanner = new Scanner(System.in); //declaring myScanner and we are going to receive input from the screen-construct
    //declaring and initializng the following variables
    int fourKind = 0; //Four cards of the same face value and one side card
    int threeKind = 0; //three cards of the same face value and two side card
    int twoPair = 0; //Two cards with a matching face value,
                    //another two cards with a different matching face value, and one side card.
    int onePair = 0; //Two cards with a matching face value and three side cards.
    int cardOneSuit; //suit of card 1
    int cardTwoSuit; // suit of card two
    int cardThreeSuit; //suit of card 3 
    int cardFourSuit; //suit of card 4
    int cardFiveSuit; //suit of card 5
    
    
    System.out.println("Enter number of hands: "); //asking user to enter number of hands
      //while loop if the user doesnt input an integer 
    while (!myScanner.hasNextInt()) { //checking if the input is not an int
            myScanner.next(); //input as string 
            System.out.println("Please input an integer only"); //asking the user to input an integer
    }
        int totalCount = myScanner.nextInt(); //the user input's name is totalcount
        
    for (int i = 0; i<= totalCount; i++){ //for loop for the whole program to increment i ( a counting variable) if its less than totalCount
        int firstCard = (int) (Math.random()*52 + 1); //randomly generating a number between 1 and 52 for card 1
        int secondCard = (int) (Math.random()*52 + 1); //randomly generating a number between 1 and 52 for card 2
      
        while(secondCard == firstCard){ //while card 1 and card 2 have the same value second card is randomly generated again
          secondCard = (int) (Math.random()*52 + 1);
      }
        int thirdCard = (int) (Math.random()*52 + 1); //randomly generating a number between 1 and 52 for card 3
      while((thirdCard == firstCard)||(thirdCard == secondCard)){ //while card 1 and card 3 and card 3 and card 2 have the same values 3rd card is randomly generated again
          thirdCard = (int) (Math.random()*52 + 1);
      }  
        int fourthCard = (int) (Math.random()*52 + 1); //randomly generating a number between 1 and 52 for card 4
        while((fourthCard == firstCard)||(fourthCard == secondCard)||(fourthCard == thirdCard)){ //while card 1 and card 4 and card 4 and card 2 and card 4 and 3 have the same values 4th card is randomly generated again
             fourthCard = (int) (Math.random()*52 + 1); 
        }
        int fifthCard = (int) (Math.random()*52 + 1);//randomly generating a number between 1 and 52 for card 5
        while((fifthCard == firstCard)||(fifthCard == secondCard)||(fifthCard == thirdCard)){ //while card 1 and card 5 and card 5 and card 2 and card 5 and 3 have the same values 5th card is randomly generated again
            fifthCard = (int) (Math.random()*52 + 1);
        }
        //suits of each card 
        cardOneSuit = (firstCard)/13;
        cardTwoSuit = (secondCard)/13;
        cardThreeSuit = (thirdCard)/13;
        cardFourSuit = (fourthCard)/13;
        cardFiveSuit = (fifthCard)/13;
       
        //values of each card inside each suit
        firstCard = (firstCard)%13;
        secondCard = (secondCard)%13;
        thirdCard = (thirdCard)%13;
        fourthCard = (fourthCard)%13;
        fifthCard = (fifthCard)%13;
        
         ////all the possibilities that Four cards of the same face value and one side card could occur  
        if ((firstCard == secondCard) && (secondCard == thirdCard) && (thirdCard == fourthCard) || 
            (firstCard == secondCard) && (secondCard == thirdCard) && (thirdCard == fifthCard) ||
            (firstCard == secondCard) && (secondCard == fourthCard) && (fourthCard == fifthCard) ||
            (secondCard == thirdCard) && (thirdCard == fourthCard) && (fourthCard == fifthCard))  {
               fourKind++;
               //System.out.println("Four-of-a-kind");
               //fourCount = true;
            
        }
       //all the possibilities that three cards of the same face value and two side card could occur 
       //if (fourCount == false){
        else if ((firstCard == secondCard && firstCard == thirdCard)||
                   (firstCard == thirdCard && firstCard == fourthCard)||
                   (firstCard == fourthCard && firstCard == fifthCard)||
                   (secondCard == firstCard && secondCard == thirdCard)||
                   (thirdCard == firstCard && thirdCard == secondCard)||
                   (thirdCard == fourthCard && thirdCard == fifthCard)||
                   (fourthCard == firstCard && fourthCard == secondCard)||
                   (fourthCard == thirdCard && firstCard == fifthCard)||
                   (fifthCard == firstCard && fifthCard == secondCard)||
                   (fifthCard == thirdCard && firstCard == fourthCard))
           {
               threeKind++;
               //Use this to test code
             //  System.out.println("Three-of-a-Kind");
              // threeCount = true;
           }
           
      // } 
        //all the possibilities that Two cards with a matching face value, another two cards with a different matching face value, and one side card could occur
        else if (((firstCard == secondCard)&&(thirdCard == fourthCard))||
                ((firstCard == thirdCard)&&(secondCard == fourthCard))||
                ((firstCard == fourthCard)&&(secondCard == thirdCard))||
                ((firstCard == secondCard)&&(thirdCard == fifthCard))||
                ((firstCard == thirdCard)&&(secondCard == fifthCard))||
                ((firstCard == fourthCard)&&(thirdCard == fifthCard))||
                ((firstCard == thirdCard)&&(secondCard == fifthCard))||
                ((thirdCard == secondCard)&&(fifthCard == fourthCard))||
                ((firstCard == thirdCard)&&(fifthCard == fourthCard)))
            {
                twoPair++;
                
                //Use this to test code
                //System.out.println("Two-pair");
                //twoCount = true;
            }
        //all the posibilities that Two cards with a matching face value and three side cards could occur.
        //if (twoCount == false){    
        else if ((firstCard == secondCard)||
                (firstCard == thirdCard)||
                (firstCard == fourthCard)||
                (firstCard == fifthCard)||
                (secondCard == thirdCard)||
                (secondCard == fourthCard)||
                (secondCard == fifthCard)||
                (thirdCard == fourthCard)||
                (thirdCard == fifthCard)||
                (fourthCard == fifthCard))
                {
                    onePair++;
                    
                    //Use this to test code
                //System.out.println("One pair!!!!!");
                //oneCount = true;
                
                }
            
        //}
        
               
    }
    //printing out number of loops(user inputted integer)
     System.out.println("The number of loops: " + totalCount); 
      //probability of a fourKind rounded to 3 decimal points
     System.out.println("The probability of Four-of-a-kind: " + new java.text.DecimalFormat("0.000").format(((double)fourKind / totalCount)));
       //probability of a threeKindKind rounded to 3 decimal points
     System.out.println("The probability of Three-of-a-kind: " + new java.text.DecimalFormat("0.000").format(((double)threeKind/ totalCount)));
       //probability of a twoPair rounded to 3 decimal points
     System.out.println("The probability of Two-pair: " + new java.text.DecimalFormat("0.000").format(((double)twoPair/ totalCount)));
       //probability of a onePair rounded to 3 decimal points
     System.out.println("The probability of One-pair: " + new java.text.DecimalFormat("0.000").format(((double)onePair/ totalCount)));
   
   
   
 }
}

