/*
  Omid Ghaziazdeh      
  CSE 002
  Omid Ghazizadeh 110
  Professor Carr
  11/26/18 
  prompt the user to enter 15 ints for students’ final grades in CSE2.
  Check that the user only enters ints, in range from 0-100
  and print an error message, the int greater than the last input.
  Use binary search and linear search and scramble the array.
*/
import java.util.*; //import every class


public class CSE2Linear {

   
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in); //Scannner class
        int firstInput;
        int nextInput = 0; 
        int[] input = new int[15]; //array input of 15 elements
        System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
        for(int i = 0; i < 15; i++){
             while (true) {// while loop that always run
        if (myScanner.hasNextInt()) { // begings if statement to check if user input is an int
          firstInput = myScanner.nextInt();// putting user input into first input
          if (firstInput <= 100 && firstInput >= 0) { // if statement for if input is within range
            if (firstInput >= nextInput) {// if statement for when firstinput is greater than second input
              input[i] = firstInput;// put first input into array
              nextInput = input[i];// puts first input as second input
              break;
            } // ends if statement
            else {// for when user doesnt enter in ascending order
              System.out.println("Inputs have to be in ascending order. Try agin.");// prints error message
            } // ends else statement

          } // ends if statemnet

          else {
            System.out.println("Inputs have to be within the range of 0-100. Try again."); // prints error message
          } // ends else statement
        } // ends if statement

        else {
          myScanner.next();// promts user input
          System.out.println("Inputs have to be integers. Try again.");// prints error message
        } // ends else statement
      } // ends while loop
    } // ends for loop
        for (int i = 0; i < input.length; i++){ //for loop to print each element value in the array
            System.out.print(input[i] + " ");
        }
        System.out.println();
        System.out.print("Enter a grade to search for: "); //asknig user to enter an int
        int search = myScanner.nextInt(); //putting userinput into search variable
        int howMany = binarySearch(search, input); //assigning the return of the method to howmany
        if (howMany > 0){ //howMany is the number of iterations
            System.out.println(search + " was found in the list with " + howMany + " iterations");
        }
        else
            System.out.println(search + " was not found in the list with " + -howMany + " iterations");
        Scramble(input);
     for (int i = 0; i < input.length; i++){ //for loop to print each element value in the array
            System.out.print(input[i] + " ");// for loop for printing out array
        }
     System.out.println();
     System.out.print("Enter a grade to search for: "); //asknig user to enter an int
     int grade = myScanner.nextInt(); //putting userinput into grade variable
     
     int linear = linearSearch(input, grade); //assigning the return of the method to linear
        if (linear > 0){ //linear is the number of iterations
            System.out.println(grade + " was found in the list with " + linear + " iterations");
        }
        else
            System.out.println(grade + " was not found in the list with " + -linear + " iterations");
        }
    
    
    
    
    
    public static int binarySearch(int search, int[] input) {// This method was used from class demos
    int low = 0;
    int high = input.length - 1;
    int count = 0;
    while (high >= low) {
      int middle = (low + high) / 2;// assign middle to the middle element of the array
       count++;
      if (input[middle] == search) {// if the number is equal to middle value
        return count;// returns count number

      }
      if (input[middle] < search) {// if number is less than middle value, go to the left of mid
        low = middle + 1;
      }
      if (input[middle] > search) {// if number more than middle value go to right of mid
        high = middle - 1; 
      }
       
    }
    return -count;// returns count as negative if the number wasnt found in the array

  }// end of method
   

    public static void Scramble(int[] input){ //scramble method
        
        Random random = new Random();
        for(int i = 0; i < input.length; i++){ 
            int x = input[i]; //initializing x as the value of each element
            int y = random.nextInt(input.length); //y is a random number (0-15)
            input[y] = x; //x assigned to the value of index y
            input[i] = input[y]; //assign the value of index y to index i
            
        }
        System.out.println("Scrambeled: ");
        //return input; //return the input array
    }
    
    
     public static int linearSearch(int input[], int grade) {// some Copied from class demos 
    int count = 0;
    for (int i = 0; i < input.length; i++) {// as i approaches length of array
      count++;// adds 1 to count
      if (input[i] == grade) {// if value in array equals to grade
        return count;// return number of iterations
      } // ends if statement

    } // end of for loop
    return -count;// returns iterations as negative if number not in array
  }// end of method
    }


