/*
  Omid Ghaziazdeh      
  CSE 002
  Omid Ghazizadeh 110
  Professor Carr
  11/26/18
 randomInput(), delete(list,pos), and remove(list,target).
 Write these methods. There is a use of arrays and doing 
 different tasks with them.
*/import java.util.Random; //import Random class
import java.util.Scanner; //import Scanner class

public class RemoveElements{
  public static void main(String [] arg){ 
	Scanner scan=new Scanner(System.in); //scan being declared
int num[]=new int[10]; //array num of size 10
int newArray1[]; //declaring arrays
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput(); //num equalling to method randomInput
  	String out = "The original array is:"; 
  	out += listArray(num); //out increasing by the return of listArray method return
  	System.out.println(out); //printing out out
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt(); //index letting user to input an integer
  	newArray1 = delete(num,index); //new array1 equals to method delete with following inputs
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1); //printing out out1
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt(); //target letting user to input an integer
  	newArray2 = remove(num,target); //newArray2 equals to the method remove with following inputs
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2); //printing out out2
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next(); 
        //if y or Y is picked, the whole thing will repeat
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){ //method listArray
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  
  public static int[] randomInput(){ //method randomInput
       Random random = new Random(); 
      int[] array = new int[10]; //array with size 10
      for (int i = 0; i < array.length; i++ ){ //for loop to print random numbers between 0 and 10
          array[i] = random.nextInt(10);
      }
      return array; //returning the array
  }
  
  public static int[] remove(int[] list, int target) { //method remove
      int count1 = 0; //counting how many times target is equals to any number in the array
      for (int i = 0; i < list.length; i++){ //using a for loop to check every element
          if (list[i] == target){
              count1++;
          }
      }
      int count2 = 0; //second counter
      int[] array = new int[list.length-count1]; //an array of the size list.length-count1
    for (int i = 0; i < list.length; i++){ //for loop to check original array for every element and continueing if true
         if (list[i] == target){
              continue;
          }
         array[count2] = list[i] ; //the lements of array equalling to elements of list array
          count2++;

         
    } 
    return array;
  }
  public static int[] delete(int[] list, int pos) { //method delete
      int[] array = new int[list.length-1]; //a new array with size 1 less than list
      int count1 = 0; //counter to check the element number and pos
      for (int i = 0; i < list.length; i++){ //for loop to check all elements
          if (i == pos){ //if true continue
            continue;  
              
          }
          array[count1] = list[i]; // assigning list element values that dont equal to pos element number to array element values
          count1++;
  }
      return array;
}
}
