/*
  Omid Ghaziazdeh      
  CSE 002 110
  Professor Carr
  10/18/18
  developing a program to hide the secret message X.
  Instead of printing out X which everyone can see
  clearly, we will bury the character in a handful of stars.
*/


import java.util.Scanner; //importing Scanner class


public class EncryptedX {

   
   public static void main (String [] args) { //main method
        Scanner myScanner = new Scanner(System.in); //setting up an input scanner
        int input; //declaring input
        System.out.println("Input an integer between 0 and 100:"); //asking user this
        while (!myScanner.hasNextInt()) { //checking if the user input is an integer
            myScanner.next(); //allowing user to input String
            System.out.println("Please input an integer only"); //telling user to input again
        }
        input = myScanner.nextInt(); //getting the int value from the user input
        
        while (input > 100){  //checking if the input is less than 100 
            System.out.println("Please input an integer between 0 and 100 only");
           input = myScanner.nextInt();
        }
        
            //prints number of rows for x being less than the input
            //and equal to 0 to start. then incrementing it until it reaches input value.
            for (int x = 0; x < input; x++){ 
                    //number of columns for y being less than input
                    //and to start at 0. Increment y until it reaches input value
                    //then go through the loop again and check conditions.
                for (int y = 0; y < input; y++ ){ 
                    // print a space if the x and y values are the same or 
                    //if the x+1 is equal to the number inputted -1
                    if (x == y || (x + 1) == input - y){
                        System.out.print(" ");
                    }
                    // if the if condition isnt true then print a star.
                    else {
                        System.out.print("*");
                    }
            }
                //creating a new line after each loop to exactly reach the dimension
                //as the number inputted
                System.out.println();
        }
    }
}

    
